const desingScreenWidth = 1440;
const desingScreenHeight = 900;

const GetPercentageForResponsiveWidth = (pixels) => {
  const actualSize = window.innerWidth;
  const realPixels = actualSize * pixels / desingScreenWidth;
  return realPixels * 100 / actualSize;
}
const GetPercentageForResponsiveHeight = (pixels) => {
  const actualSize = window.innerHeight;
  const realPixels = actualSize * pixels / desingScreenHeight;
  return realPixels * 100 / actualSize;
}
export {
  GetPercentageForResponsiveHeight,
  GetPercentageForResponsiveWidth
}
