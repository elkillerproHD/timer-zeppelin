import React, { useState, useEffect, useRef } from 'react';
import styled from 'styled-components';
import {GetPercentageForResponsiveHeight, GetPercentageForResponsiveWidth} from "../../utils/sizes";
import SvgLogo from "../../icons/js/logo.svg";
import Ripples from 'react-ripples'
import {act} from "@testing-library/react";

const CardWith = GetPercentageForResponsiveWidth(726)

const Container = styled.div `
  width: ${CardWith}vw;
  height: ${CardWith * (450 * 100 / 726 / 100)}vw;
  border: 1px solid rgba(0, 0, 0, 0.31);
  border-radius: 20px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 700px) {
    width: ${GetPercentageForResponsiveWidth(1100)}vw;
    height: ${CardWith * (600 * 100 / 726 / 100)}vw;
  }
  @media (max-width: 450px) {
    //flex-direction: column;
    justify-content: center;
    height: ${CardWith * (700 * 100 / 726 / 100) * 0.9}vw;
    padding-bottom: ${CardWith * (700 * 100 / 726 / 100) * 0.1}vw;
  }
  @media (max-width: 357px) {
    flex-direction: column;
    justify-content: center;
    width: 80vw;
    height: 50vh;
  }
`
const Footer = styled.div `
  width: ${CardWith}vw;
  height: 4vw;
  background-color: #F8F8F8;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
  position: absolute;
  bottom: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 700px) {
    width: ${GetPercentageForResponsiveWidth(1100)}vw;
    height: 6vw;
  }
  @media (max-width: 357px) {
    width: 80vw;
    height: 5vh;
  }
`
const FooterText = styled.p `
  color: #000;
  font-family: Roboto;
  font-size: 1.2vw;
  margin: 0;
  padding: 0;
  margin-left: 1vw;
  @media (max-width: 700px) {
    font-size: 2vw;
  }
  @media (max-width: 357px) {
    font-size: 4vw;
  }
`
const Header = styled.div `
  position: absolute;
  background-color: white;
  top: -1vw;
  left: 0;
  right: 0;
  margin: auto;
  width: ${GetPercentageForResponsiveWidth(250)}vw;
  height: 2vw;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 700px) {
    width: ${GetPercentageForResponsiveWidth(310)}vw;
  }
  @media (max-width: 357px) {
    width: 62vw;
  }
`
const HeaderText = styled.p `
  color: #000;
  font-family: Roboto;
  font-size: 1.3vw;
  margin: 0;
  padding: 0;
  @media (max-width: 700px) {
    font-size: 1.8vw;
  }
  @media (max-width: 357px) {
      font-size: 5.2vw;
  }

`
const Square = styled.div `
  width: ${GetPercentageForResponsiveWidth(205)}vw;
  height: ${GetPercentageForResponsiveWidth(205) * (75 * 100 / 205 / 100)}vw;
  box-shadow: inset 0px 4px 4px rgba(0, 0, 0, 0.04);
  border-radius: 8px;
  border: 1px solid rgba(0, 0, 0, 0.13);
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 700px) {
    width: ${GetPercentageForResponsiveWidth(240)}vw;
    height: ${GetPercentageForResponsiveWidth(205) * (90 * 100 / 205 / 100)}vw;
  }
  @media (max-width: 357px) {
    width: 60vw;
    height: 15vw;
  }
`
const ButtonLayout = styled.div `
  width: ${GetPercentageForResponsiveWidth(205)}vw;
  height: ${GetPercentageForResponsiveWidth(205) * (50 * 100 / 205 / 100)}vw;
  border-radius: 8px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.09);
  cursor: pointer;
  margin-top: 1vw;
  @media (max-width: 700px) {
    width: ${GetPercentageForResponsiveWidth(240)}vw;
    height: ${GetPercentageForResponsiveWidth(205) * (70 * 100 / 205 / 100)}vw;
  }
  @media (max-width: 450px) {
    margin-top: 0;
  }
  @media (max-width: 357px) {
    width: 60vw;
    height: 8vw;
    margin-top: 2vw;
  }
`
const Button = styled.div `
  width: ${GetPercentageForResponsiveWidth(205)}vw;
  height: ${GetPercentageForResponsiveWidth(205) * (50 * 100 / 205 / 100)}vw;
  border-radius: 8px;
  background-color: #0094FF;
  display: flex;
  justify-content: center;
  align-items: center;
  @media (max-width: 700px) {
    width: ${GetPercentageForResponsiveWidth(240)}vw;
    height: ${GetPercentageForResponsiveWidth(205) * (70 * 100 / 205 / 100)}vw;
  }
  @media (max-width: 357px) {
    width: 60vw;
    height: 8vw;
  }
`
const ButtonText = styled.p `
  color: white;
  font-family: Roboto;
  font-size: 1.3vw;
  margin: 0;
  padding: 0;
  @media (max-width: 700px) {
    font-size: 2vw;
  }
  @media (max-width: 357px) {
    font-size: 4vw;
  }
`
const Divider = styled.div `
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 0 1vw 1vw 1vw;
  @media (max-width: 450px) {
    margin: 0 2vw;
  }
  @media (max-width: 357px) {
    margin: 4vw 0;
  }
`
const SquareText = styled.div `
  color: #000;
  font-family: Roboto;
  font-size: 2vw;
  margin: 0;
  padding: 0;
  @media (max-width: 700px) {
    font-size: 3vw;
  }
  @media (max-width: 357px) {
    font-size: 7vw;
  }

`
const SecondarySquareText = styled.div `
  color: #a2a2a2;
  font-family: Roboto;
  font-size: 1.3vw;
  margin: 0;
  padding: 0;
  padding-bottom: 0.2vw;
  @media (max-width: 700px) {
    font-size: 1.8vw;
    padding-bottom: 0.4vw;
  }
  @media (max-width: 357px) {
    font-size: 4vw;
    padding-bottom: 0.6vw;
  }
`
const TimerTextContainer = styled.div `
  display: flex;
  justify-content: center;
  align-items: flex-end;
`
const Logo = styled.svg `
  width: 2vw;
  height: 2vw;
  @media (max-width: 700px) {
    width: 3vw;
    height: 3vw;
  }
  @media (max-width: 357px) {
    width: 6vw;
    height: 6vw;
  }

`

const ButtonComp = (props) => {
  return (
    <ButtonLayout >
      <Ripples onClick={props.onClick}>
        <Button>
          <ButtonText>
            {props.children}
          </ButtonText>
        </Button>
      </Ripples>
    </ButtonLayout>
  )
}

let crono;


const MainCard = (props) => {
  let [ count, setCount ] = useState(0);
  let [ hours, setHours ] = useState(0);
  let [ minutes, setMinutes ] = useState(0);
  let [ seconds, setSeconds ] = useState(0);
  const [ inMarch, setMarch ] = useState(false);
  const UpgradeCount = () => {
    setCount(count + 1);
  }
  const timeFunc = () => {
    setSeconds(newSecond => {
      if(newSecond > 58) {
        setSeconds(0)
        setMinutes(newMinutes => {
          if(newMinutes > 58){
            setMinutes(0)
            setHours(newHours => {
              if(newHours > 22) {
                return 0
              } else {
                return newHours + 1
              }
            })
          } else {
            return newMinutes + 1
          }
        })
      } else {
        return newSecond + 1
      }
    })
  }

  const StartTiming = () => {
    if(!inMarch){
      setHours(0);
      setMinutes(0);
      setSeconds(0);
      setMarch(true);
      crono = setInterval(timeFunc, 1000)
    } else {
      clearInterval(crono);
      setMarch(false);
    }
  }
  return(
    <Container>
      <Header >
        <HeaderText><b>EJERCICIO:</b> <span style={{color: "#c2c2c2"}}>BOTONERA</span></HeaderText>
      </Header>
      <Divider>
        <Square>
          <SquareText>{ count }</SquareText>
        </Square>
        <ButtonComp onClick={UpgradeCount}>
          Contar
        </ButtonComp>
      </Divider>
      <Divider>
        <Square>
          <TimerTextContainer>
            <SquareText>{hours}</SquareText>
            <SecondarySquareText>h</SecondarySquareText>
          </TimerTextContainer>
          <TimerTextContainer>
            <SquareText>{minutes}</SquareText>
            <SecondarySquareText>m</SecondarySquareText>
          </TimerTextContainer>
          <TimerTextContainer>
            <SquareText>{seconds}</SquareText>
            <SecondarySquareText>s</SecondarySquareText>
          </TimerTextContainer>
        </Square>
        <ButtonComp onClick={StartTiming}>
          {
            inMarch ? (
              "Pausar Timer"
            ) : (
              "Iniciar Timer"
            )
          }

        </ButtonComp>
      </Divider>

      <Footer >
        <Logo viewBox="0 0 32 23" fill="none" >
          <SvgLogo />
        </Logo>
        <FooterText><b>zeppelin</b>labs</FooterText>
      </Footer>
    </Container>
  )
}

export default MainCard;
