import React from 'react';
import styled from 'styled-components';
import MainCard from "./components/card";

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  background-color: white;
  display: flex;
  justify-content: center;
  align-items: center;
`

function App() {
  return (
    <Container>
      <MainCard />
    </Container>
  );
}

export default App;
